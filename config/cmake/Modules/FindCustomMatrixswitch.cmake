include(LibommFindPackage)

libomm_find_package(matrixswitch
  REQUIRED
  MIN_VERSION 1.2.0
  GIT_REPOSITORY "https://gitlab.com/ElectronicStructureLibrary/omm/matrixswitch"
  GIT_TAG "master"
  SOURCE_DIR ${MATRIXSWITCH_SOURCE_DIR}
  )
