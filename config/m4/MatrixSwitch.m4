# -*- Autoconf -*-
#
# M4 macros for libOMM
#
# Copyright (C) 2016 Yann Pouillon
#
# This file is part of the libOMM software package. For license information,
# please see the COPYING file in the top-level directory of the libOMM source
# distribution.
#

#
# MatrixSwitch support
#



# OMM_MSW_DETECT()
# -------------------
#
# Checks that the selected linear algebra libraries properly work.
#
AC_DEFUN([OMM_MSW_DETECT],[
  dnl Init
  omm_msw_ok="unknown"

  dnl Prepare environment
  saved_CPPFLAGS="${CPPFLAGS}"
  saved_FCFLAGS="${FCFLAGS}"
  saved_LIBS="${LIBS}"
  CPPFLAGS="${CPPFLAGS} ${omm_msw_incs}"
  FCFLAGS="${FCFLAGS} ${CPPFLAGS}"
  LIBS="${omm_msw_libs} ${LIBS}"

  dnl Decide which tests to perform
  omm_msw_sequence="1"
  if test "${omm_mpi_ok}" = "yes" -a "${omm_linalg_has_lapack}" = "yes" -a \
          "${omm_linalg_has_scalapack}" = "yes"; then
    omm_msw_sequence="1 2 3"
  fi

  dnl Check MatrixSwitch routine
  for omm_msw_test in ${omm_msw_sequence}; do
    case "${omm_msw_test}" in
      1)
       omm_msw_extra_libs=""
       ;;
      2)
       omm_msw_extra_libs="-ldbcsr"
       ;;
      3)
       omm_msw_extra_libs="-ldbcsr -lxsmm"
       ;;
    esac
    LIBS="${omm_msw_libs} ${omm_msw_extra_libs} ${saved_LIBS}"
    AC_LANG_PUSH([Fortran])
    AC_MSG_CHECKING([whether the MatrixSwitch library works (extra_libs: '${omm_msw_extra_libs}')])
    AC_LINK_IFELSE([AC_LANG_PROGRAM([],
      [[
        use matrixswitch
        character(3) :: m_operation
        double precision :: x_min
        type(matrix) :: HW, HWd
        call m_add(HWd, 'c', HW, x_min, 1.0d0, m_operation)
      ]])], [omm_msw_ok="yes"], [omm_msw_ok="no"])
    AC_MSG_RESULT([${omm_msw_ok}])
    AC_LANG_POP([Fortran])
    test "${omm_msw_ok}" = "yes" && break
  done

  dnl Adjust MatrixSwitch configuration
  omm_msw_libs="${omm_msw_libs} ${omm_msw_extra_libs}"

  dnl Restore environment
  CPPFLAGS="${saved_CPPFLAGS}"
  FCFLAGS="${saved_FCFLAGS}"
  LIBS="${saved_LIBS}"
]) # OMM_MSW_DETECT
