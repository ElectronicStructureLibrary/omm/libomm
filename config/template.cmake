@PACKAGE_INIT@

#- Search for dependencies

if(NOT TARGET matrixswitch::matrixswitch)

 list(PREPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}")

 #
 # matrixswitch_DIR is a hint for cmake-package search: if set, and existing,
 # is a guarantee that we will find the dependency we compiled the library with
 #
 # matrixswitch_PREFIX is set if matrixswitch was discovered using a pkg-config search.
 #
 # In all cases, the logic in the custom finder module takes over. This means
 # that 'cmake' search might take precedence, even if matrixswitch_PREFIX is set.
 #
 # The following can be made less verbose
 set(matrixswitch_DIR @matrixswitch_DIR@)
 set(matrixswitch_PREFIX @matrixswitch_PREFIX@)

 set(_searched FALSE)

 if(matrixswitch_DIR)
  message(STATUS "Recorded hint for matrixswitch cmake-package search: ${matrixswitch_DIR}")
  if (EXISTS "${matrixswitch_DIR}")
   set(matrixswitch_FIND_METHOD "cmake")
   find_package(CustomMatrixswitch)
   set(_searched TRUE)
  else()
   message(STATUS "... but that directory does not exist... you might need extra hints")
  endif()	    

 elseif(matrixswitch_PREFIX)
  message(STATUS "Recorded hint for pkg-conf matrixswitch_PREFIX: ${matrixswitch_PREFIX}")

  if (EXISTS "${matrixswitch_PREFIX}")

   list(PREPEND CMAKE_PREFIX_PATH "${matrixswitch_PREFIX}")
   set(matrixswitch_FIND_METHOD "pkgconf")
   find_package(CustomMatrixswitch)
   list(POP_FRONT CMAKE_PREFIX_PATH)
   set(_searched TRUE)

  else()
   message(STATUS "... but that directory does not exist... you might need extra hints")
  endif()

 endif()

 if (NOT _searched)
  message(STATUS "No matrixswitch dependency hints recorded. Finder module takes over")
  find_package(CustomMatrixswitch)
 endif()

 list(POP_FRONT CMAKE_MODULE_PATH)

endif()

#-------

if(NOT TARGET "@PROJECT_NAME@::@PROJECT_NAME@")
  include("${CMAKE_CURRENT_LIST_DIR}/@PROJECT_NAME@-targets.cmake")
endif()

