#!/bin/sh

# GNU - OpenMPI - MKL

if test "${MKLROOT}" = ""; then
    echo "helper: error: MKLROOT is not defined" >&2
    exit 1
fi

MKL_INCS="-m64 -I${MKLROOT}/include"
MKL_LIBS="${MKLROOT}/lib/intel64/libmkl_scalapack_lp64.a \
    -Wl,--start-group \
    ${MKLROOT}/lib/intel64/libmkl_gf_lp64.a \
    ${MKLROOT}/lib/intel64/libmkl_sequential.a \
    ${MKLROOT}/lib/intel64/libmkl_core.a \
    ${MKLROOT}/lib/intel64/libmkl_blacs_openmpi_lp64.a \
    -Wl,--end-group -lpthread -lm -ldl"

../configure \
    MPICC="mpicc" \
    MPICXX="mpic++" \
    MPIFC="mpifort" \
    LINALG_INCLUDES="${MKL_INCS}" \
    LINALG_LIBS="${MKL_LIBS}" \
    "$@"
