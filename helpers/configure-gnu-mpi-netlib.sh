#!/bin/sh

# GNU - OpenMPI - Netlib

../configure \
    MPICC="mpicc" \
    MPICXX="mpic++" \
    MPIFC="mpifort" \
    LINALG_LIBS="-lscalapack${NETLIB_SUFFIX} -llapack -lblas" \
    "$@"
